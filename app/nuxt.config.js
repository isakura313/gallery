import translate from "./translate";
export default {
 ssr: false,
 loading: true,

 // Target (https://go.nuxtjs.dev/config-target)
 target: "static",

 // Global page headers (https://go.nuxtjs.dev/config-head)
 head: {
  title: "PIXELS CLONE",
  meta: [
   {charset: "utf-8"},
   {name: "viewport", content: "width=device-width, initial-scale=1"},
   {
    hid: "description",
    name: "description",
    content: "галерея, фотография, API, pexels",
   },
  ],
  link: [{rel: "icon", type: "image/png", href: "/favicon.png"}],
 },

 css: [],

 // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
 plugins: [],

 // Auto import components (https://go.nuxtjs.dev/config-components)
 components: true,

 // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
 buildModules: [],

 // Modules (https://go.nuxtjs.dev/config-modules)
 modules: [
  // https://go.nuxtjs.dev/buefy
  // https://go.nuxtjs.dev/axios
  "@nuxtjs/i18n",
  "@nuxtjs/vuetify",
  "@nuxtjs/axios",
  "@nuxtjs/proxy",
 ],
 i18n: {
  strategy: 'prefix',
  locales: ['en', 'ru', 'he'],
  defaultLocale: 'en',
  vueI18n: {
   fallbackLocale: 'en',
   messages: {
    en: translate.en,
    ru: translate.ru,
    he: translate.he
   }
  }
 },
 router: {
  base: '/gallery/'
 },

 // Axios module configuration (https://go.nuxtjs.dev/config-axios)
 axios: {
  proxy: true,
 },
 proxy: {
  "/user/": {
   target: "http://localhost:3081/user/",
   pathRewrite: {"^/user/": ""},
   changeOrigin: false,
  },
 },

 // Build Configuration (https://go.nuxtjs.dev/config-build)
 build: {},
};
