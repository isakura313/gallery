const ru = {
 home: "Домой",
 search:"Поиск",
 collection: "Избранное",
 photo_on_taste: "Фотографии на любой вкус",
 looking_for: "Здесь пока ничего нет... Ищите",
 search_on: "Поиск на",
 result_search: "Результат поиска по запросу",
 your_favorite_photo: "Ваши избранные фотографии",
 add_a_thread: "Здесь слишком пусто..."
}

export default ru;
