const he = {
 home: "בית",
 search: "חיפוש",
 collection: "אוסף",
 photo_on_taste: "תמונות לכל טעם",
 looking_for: "עדיין אין כאן כלום... חפש",
 search_on: "חיפוש ב",
 result_search: "תוצאות חיפוש לפי שאילתת חיפוש",
 your_favorite_photo: "התמונות המועדפות עליך",
 add_a_thread: "הוסף כאן חוט, הוא ריק מדי"
}

export default he;
