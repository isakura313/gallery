import en from './en';
import ru from './ru'
import he from './he';

const messages = {
 ru,
 en,
 he
}
export default messages;
